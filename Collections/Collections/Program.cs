﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)

        {
            //create a collection
            var ListOfSuperHeroes = new List<SuperHero>();
            var ListOfSuperHeroes2 = new List<SuperHero>();
          //  ShowSuperHeroes(ListOfSuperHeroes);
            ListOfSuperHeroes = GetFiveSuperHeroes();
            var temphero1 = new SuperHero();
            temphero1.age = 21;
            temphero1.brand = "Dc";
            temphero1.name = "Akku";
            temphero1.power = "Sleeping";

            var temphero2 = new SuperHero();
            temphero2.age = 21;
            temphero2.brand = "Dc";
            temphero2.name = "Gana";
            temphero2.power = "Sleeping";

            ListOfSuperHeroes2.Add(temphero1);
            ListOfSuperHeroes2.Add(temphero2);
            //ShowSuperHeroes(ListOfSuperHeroes2);
            //ShowSuperHeroes(ListOfSuperHeroes);

            var finallist = new List<SuperHero>();
            finallist.AddRange(ListOfSuperHeroes);
            finallist.AddRange(ListOfSuperHeroes2);
            ShowSuperHeroes(finallist);
            int age = 43;
            String name = "Akku";
            SuperHero returnhero = findByAge(name,finallist);

              Console.ReadLine();
        }

        private static SuperHero findByAge(String name, List<SuperHero> finallist)
        {
            SuperHero temphero = new SuperHero();
            //var temphero = listOfSuperHeroes.Select(x => x).Where(x => x.age == 10);
           // temphero = finallist.Select(x => x).Where(x => x.age == age).FirstOrDefault();
            temphero = finallist.Select(x => x).Where(x => x.name== name).FirstOrDefault();
            return temphero;
        }

        private static void ShowSuperHeroes(List<SuperHero> listOfSuperHeroes)
        {
            //.OrderByDescending(x => x.Delivery.SubmissionDate);
            //listOfSuperHeroes.OrderByDescending(x => x.age);
            listOfSuperHeroes = listOfSuperHeroes.OrderByDescending(x => x.age).ToList();
            foreach (var x in listOfSuperHeroes)
            {
                Console.WriteLine("------------------");
                Console.WriteLine("Name " + x.name);
                Console.WriteLine("Age " + x.age);
                Console.WriteLine("Power  " + x.power);
                Console.WriteLine("Brand " + x.brand);
                Console.WriteLine("------------------");
            }
        }
        private static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();

            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new SuperHero();
            var tempHero2 = new SuperHero();
            var tempHero3 = new SuperHero();
            var tempHero4 = new SuperHero();
            var tempHero5 = new SuperHero();
            var tempHero6 = new SuperHero();


            name = "Jay";
            power = "Money";
            brand = "Apple";
            age = 18;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);


            name = "Soumik";
            power = "Fighting";
            brand = "DC";
            age = 37;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "Pankaj";
            power = "Cheating";
            brand = "DC";
            age = 43;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "Tanay";
            power = "Money";
            brand = "DC";
            age = 13;

            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "Hussain";
            power = "Money";
            brand = "DC";
            age = 62;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);


            return tempList;
        }
    }
}