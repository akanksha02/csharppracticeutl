﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MCVDemo6.Startup))]
namespace MCVDemo6
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
