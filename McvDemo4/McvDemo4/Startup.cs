﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(McvDemo4.Startup))]
namespace McvDemo4
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
