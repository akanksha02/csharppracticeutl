﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Mcvdemo3.Startup))]
namespace Mcvdemo3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
