﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mvcdemo1
{
    public class SuperHero
    {
        public String name { get; set; }
        public String power { get; set; }
        public String brand { get; set; }
        public int age
        {
            get; set;
        public static void ShowSuperHeroes(List<SuperHero> listOfSuperHeroes)
        {
            //.OrderByDescending(x => x.Delivery.SubmissionDate);
            //listOfSuperHeroes.OrderByDescending(x => x.age);
            listOfSuperHeroes = listOfSuperHeroes.OrderByDescending(x => x.age).ToList();
            foreach (var x in listOfSuperHeroes)
            {
                Console.WriteLine("------------------");
                Console.WriteLine("Name " + x.name);
                Console.WriteLine("Age " + x.age);
                Console.WriteLine("Power  " + x.power);
                Console.WriteLine("Brand " + x.brand);
                Console.WriteLine("------------------");
            }
        }
}