﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(mvcdemo1.Startup))]
namespace mvcdemo1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
